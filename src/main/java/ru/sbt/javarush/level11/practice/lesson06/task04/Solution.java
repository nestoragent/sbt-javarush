package ru.sbt.javarush.level11.practice.lesson06.task04;

/* Все мы работники
Написать четыре класса: Employee(сотрудник), Manager(управляющий), Chief(директор) и  Secretary(секретарь).
Унаследовать управляющего, директора и секретаря от сотрудника.
*/

public class Solution
{
    public class Manager
    {

    }

    public class Chief
    {

    }

    public class Employee
    {

    }

    public class Secretary
    {

    }
}
