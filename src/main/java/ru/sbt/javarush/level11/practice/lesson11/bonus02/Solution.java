package ru.sbt.javarush.level11.practice.lesson11.bonus02;

/* Нужно добавить в программу новую функциональность
Добавь общий базовый класс к классам-фигур:  (фигуры из шахмат).
*/

public class Solution
{
    public static void main(String[] args)
    {
    }

    public class King
    {
    }

    public class Queen
    {
    }

    public class Rook
    {
    }

    public class Knight
    {
    }

    public class Bishop
    {
    }

    public class Pawn
    {
    }
}
