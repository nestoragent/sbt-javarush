package ru.sbt.javarush.level11.practice.lesson11.home08;

/* Третья правильная «цепочка наследования»
Расставь правильно «цепочку наследования» в классах: Pet (домашнее животное), Cat (кот), Dog(собака), Car (машина).
*/

public class Solution
{
    public static void main(String[] args)
    {
    }

    public class Pet
    {

    }

    public class Cat
    {

    }

    public class Car
    {

    }

    public class Dog
    {

    }
}
