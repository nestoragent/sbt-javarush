package ru.sbt.javarush.level11.practice.lesson11.home07;

/* Вторая правильная «цепочка наследования»
Расставь правильно «цепочку наследования» в классах: Carnivora (плотоядное животное), Cow (корова), Dog(собака), Pig(свинья), Animal (животное).
*/

public class Solution
{
    public static void main(String[] args)
    {
    }

    public class Carnivora
    {

    }

    public class Cow
    {

    }

    public class Dog
    {

    }

    public class Pig
    {

    }

    public class Animal
    {

    }
}
