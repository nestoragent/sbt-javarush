package ru.sbt.javarush.level19.practice.lesson03.task04;

import java.io.IOException;

public interface PersonScanner {
    Person read() throws IOException;

    void close() throws IOException;
}
