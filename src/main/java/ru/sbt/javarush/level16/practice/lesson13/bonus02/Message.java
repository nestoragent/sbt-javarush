package ru.sbt.javarush.level16.practice.lesson13.bonus02;

public interface Message {
    void showWarning();
}
