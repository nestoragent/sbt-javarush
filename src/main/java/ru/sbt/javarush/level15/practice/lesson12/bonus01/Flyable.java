package ru.sbt.javarush.level15.practice.lesson12.bonus01;

public interface Flyable {
    void fly();
}
