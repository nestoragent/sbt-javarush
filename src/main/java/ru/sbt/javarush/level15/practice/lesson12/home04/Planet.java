package ru.sbt.javarush.level15.practice.lesson12.home04;

public interface Planet {
    static String SUN = "sun";
    static String MOON = "moon";
    static String EARTH = "earth";
}
