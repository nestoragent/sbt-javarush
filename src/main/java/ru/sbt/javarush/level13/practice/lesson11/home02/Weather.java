package ru.sbt.javarush.level13.practice.lesson11.home02;

public interface Weather
{
    String getWeatherType();
}
