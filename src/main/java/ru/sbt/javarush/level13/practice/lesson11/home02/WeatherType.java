package ru.sbt.javarush.level13.practice.lesson11.home02;

public interface WeatherType
{
    String CLOUDY = "Cloudy";
    String FOGGY = "Foggy";
    String FROZEN = "Frozen";
}
