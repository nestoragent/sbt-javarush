package ru.sbt.javarush.level17.practice.lesson10.bonus03;

public class Table {
    private static byte tableNumber;
    private byte number = ++tableNumber;

    public Order getOrder () {
        return new Order(number);
    }
}
