package ru.sbt.javarush.level17.practice.lesson10.home01;

import java.util.ArrayList;

/* Общий список
1. Изменить класс Solution так, чтобы он стал списком. (Необходимо реализовать интерфейс java.util.List).
2. Список Solution должен работать только с целыми числами Long.
3. Воспользуйтесь полем original.
4. Список будет использоваться нитями, поэтому позаботьтесь, чтобы все методы были синхронизированы.
*/

public class Solution {
    private ArrayList<Long> original = new ArrayList<Long>();
}
