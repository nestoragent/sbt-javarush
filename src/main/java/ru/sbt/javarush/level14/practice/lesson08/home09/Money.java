package ru.sbt.javarush.level14.practice.lesson08.home09;

public abstract class Money
{
    private static double a;

    protected Money() {
    }

    public void setA(double a) {
        this.a = a;
    }

    public abstract String getCurrencyName();
}

