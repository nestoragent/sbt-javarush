package ru.sbt.javarush.level14.practice.home01;

/**
 * Created by SBT-Yanovskiy-ID on 01.02.2016.
 */
abstract class Hen implements Country{
    abstract int getCountOfEggsPerMonth();
    String getDescription(){
        return "Я курица.";
    }
}
