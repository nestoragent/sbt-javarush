package ru.sbt.javarush.level14.practice.home01;

/**
 * Created by SBT-Yanovskiy-ID on 01.02.2016.
 */
public class BelarusianHen extends Hen{
    @Override
    int getCountOfEggsPerMonth() {
        return 700;
    }
    @Override
    String getDescription() {
        return super.getDescription() + " Моя страна - " + Country.BELARUS + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
    }
}