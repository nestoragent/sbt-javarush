package ru.sbt.javarush.level14.practice.home01;

/**
 * Created by SBT-Yanovskiy-ID on 01.02.2016.
 */
public class MoldovanHen extends Hen{
    @Override
    int getCountOfEggsPerMonth() {
        return 100;
    }
    @Override
    String getDescription() {
        return super.getDescription() + " Моя страна - " + Country.MOLDOVA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
    }
}