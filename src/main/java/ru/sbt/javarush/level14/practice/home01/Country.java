package ru.sbt.javarush.level14.practice.home01;

public interface Country
{
    String UKRAINE = "Ukraine";
    String RUSSIA = "Russia";
    String MOLDOVA = "Moldova";
    String BELARUS = "Belarus";
}
