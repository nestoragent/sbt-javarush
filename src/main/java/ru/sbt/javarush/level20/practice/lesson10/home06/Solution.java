package ru.sbt.javarush.level20.practice.lesson10.home06;

import java.io.Serializable;

/* Запрет сериализации
Запретите сериализацию класса SubSolution используя NotSerializableException.
Сигнатуры классов менять нельзя
*/
public class Solution implements Serializable {
    public static class SubSolution extends Solution {
    }
}
