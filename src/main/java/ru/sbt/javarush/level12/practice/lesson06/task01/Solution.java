package ru.sbt.javarush.level12.practice.lesson06.task01;

/* Абстрактный класс Pet
Сделать класс Pet абстрактным.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Pet
    {
        public String getName()
        {
            return "Я - котенок";
        }
    }

}